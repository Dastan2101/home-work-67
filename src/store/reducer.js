const initialPassword = {
    password: '',
    code: '1234',
    success: false,
};

const reducer = (state = initialPassword, action) => {

    switch (action.type) {
        case 'ADD_NUMBER' :
            if (state.password.length < 4) {
                return {
                    ...state,
                    password: state.password + action.number
                };
            } else {
                return {
                    ...state,
                    success: false
                }
            }
        case 'REMOVE' :
            const newPassword = state.password.substr(0, state.password.length - 1);
            return {
                ...state,
                password: newPassword
            };
        case  'CHECK' :
            if (state.password === state.code) {
                alert('Success');
                return {
                    ...state,
                    success: true,
            }
            } else {
                alert('Error')
                return {
                    ...state,
                    success: false
                }
            }

        default :
            return state
    }


};


export default reducer;