import React, {Component} from 'react';
import {connect} from 'react-redux';
import './code.css';

const arrayNumber = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

class Code extends Component {

    render() {

        let numbers = arrayNumber.map(number => (
            <button key={number} onClick={() => this.props.addNumber(number)}>{number}</button>
        ));

        return (

            <div className="code-block">
                    <input
                        className="text-area"
                        value={this.props.password}
                        disabled="disabled"
                        type="password"
                    />

                <div className="buttons"
                     style={this.props.success ? {backgroundColor: 'green'} : {backgroundColor: 'grey'}}>
                    {numbers}
                    <button onClick={this.props.removeNumber}>&lt;</button>
                    <button onClick={this.props.checkCode}>E</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        password: state.password,
        success: state.success
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addNumber: number => dispatch({type: 'ADD_NUMBER', number}),
        removeNumber: () => dispatch({type: 'REMOVE'}),
        checkCode: () => dispatch({type: 'CHECK'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Code);